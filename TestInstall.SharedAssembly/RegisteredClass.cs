﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TestInstall.SharedAssembly
{
    [Guid("2B667E2B-1294-42E7-B785-3AA273A0DD3F"), ComVisible(true)]
    public class RegisteredClass
    {
        #region ComRegistration
        [ComRegisterFunctionAttribute]
        public static void RegisterFunction(Type t)
        {
            Microsoft.Win32.RegistryKey hklm = Microsoft.Win32.Registry.LocalMachine;
            Microsoft.Win32.RegistryKey hkcu = Microsoft.Win32.Registry.CurrentUser;

            string key = "SOFTWARE\\TestInstall\\{" + t.GUID.ToString() + "}";

            Microsoft.Win32.RegistryKey addinkey = hklm.CreateSubKey(key);
            addinkey.SetValue(null, 0);
            addinkey.SetValue("Description", "Test install with Jesse");
            addinkey.SetValue("Title", "Test Install");

            key = "Software\\TestInstall\\{" + t.GUID.ToString() + "}";
            addinkey = hkcu.CreateSubKey(key);
            addinkey.SetValue(null, 1);
        }

        [ComUnregisterFunctionAttribute]
        public static void UnRegisterFunction(Type t)
        {
            Microsoft.Win32.RegistryKey hklm = Microsoft.Win32.Registry.LocalMachine;
            Microsoft.Win32.RegistryKey hkcu = Microsoft.Win32.Registry.CurrentUser;

            string key = "SOFTWARE\\TestInstall\\{" + t.GUID.ToString() + "}";

            hklm.DeleteSubKey(key);

            key = "Software\\TestInstall\\{" + t.GUID.ToString() + "}";

            hkcu.DeleteSubKey(key);
        }
        #endregion
    }
}
