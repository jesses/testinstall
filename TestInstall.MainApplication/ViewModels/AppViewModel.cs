﻿using System;
using System.Windows;
using System.Windows.Input;
using TestInstall.MainApplication.Common;

namespace TestInstall.MainApplication.ViewModels
{
    public class AppViewModel : ObservableObject
    {
        public AppViewModel() { }

        public ICommand Register
        {
            get { return new RelayCommand(RegisterExecute); }
        }

        private void RegisterExecute()
        {
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Filter = "Assembly Files |*.dll";
            ofd.InitialDirectory = System.Environment.CurrentDirectory;

            if ((bool)ofd.ShowDialog())
            {

                try
                {
                    //TODO: try and register an assembly

                    // apparently regasm is installed with .net so we should be able to use it
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public ICommand GacInstall
        {
            get { return new RelayCommand(GacInstallExecute); }
        }

        private void GacInstallExecute()
        {
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Filter = "Assembly Files |*.dll";
            ofd.InitialDirectory = System.Environment.CurrentDirectory;

            if ((bool)ofd.ShowDialog())
            {
                try
                {
                    //TODO: try and install an assembly into the gac
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
